
#include "pch.h"
#include <iostream>
#include <string.h>

struct Student {
	char* id;
	char* ime;
	char* prezime;
	char spol;
	int kviz_1;
	int kviz_2;
	int midterm;
	int final;
	int total;
};


using namespace std;
int nadi(Student niz[], int size, char* id)
{
	if (!(size))
	{
		return -2;
	}
	int i;
	for (i = 0; i < size; i++)
	{
		if (!strcmp(id, niz[i].id)) return i;
	}
	return -1;
}


double prosjek(Student niza)
{
	return niza.total / 4;
}

double prosjek(Student niz[], int size)
{
	char id[10];
	cout << "Unesite ID studenta." << endl;
	std::cin >> id;

	int i;
	i = nadi(niz, size, id);
	if (i >= 0)
	{
		return niz[i].total / 4;
	}
	return NULL;

}


void svi(Student niz[], int size)
{
	if (!(size))
	{
		cout << "Nema studenata.";
		return;
	}
	int i;
	for (i = 0; i < size; i++)
	{
		cout << "ID:"<<niz[i].id << endl;
		cout <<"Ime:"<< niz[i].ime << endl;
		cout << "Prezime:" << niz[i].prezime << endl;
		cout << "Spol:" << niz[i].spol << endl;
		cout << "Kviz 1:"<< niz[i].kviz_1 << endl;
		cout << "Kviz 2:" << niz[i].kviz_2 << endl;
		cout << "Midterm:"<<niz[i].midterm << endl;
		cout <<"Final:"<< niz[i].final << endl;
		cout << "Total:"<<niz[i].total << endl;
		cout << "----------------------------------" << endl;
		cout << endl;
	}

}

void best(Student niz[], int size)
{
	if (!(size))
	{
		cout << "Nema studenata.";
	}
	int best = 0;
	int i;
	for (i = 0; i < size; i++)
	{
		if (prosjek(niz[i]) > prosjek(niz[best]))
			best = i;
	}
	cout << "ID:" << niz[best].id << endl;
	cout << "Ime:" << niz[best].ime << endl;
	cout << "Prezime:" << niz[best].prezime << endl;
	cout << "Spol:" << niz[best].spol << endl;
	cout << "Kviz 1:" << niz[best].kviz_1 << endl;
	cout << "Kviz 2:" << niz[best].kviz_2 << endl;
	cout << "Midterm:" << niz[best].midterm << endl;
	cout << "Final:" << niz[best].final << endl;
	cout << "Total:" << niz[best].total << endl;
	cout << "----------------------------------" << endl;
	cout << endl;
	
}

void worst(Student niz[], int size)
{
	if (!(size))
	{
		cout << "Nema studenata.";
	}
	int worst = 0;
	int i;
	for (i = 0; i < size; i++)
	{
		if (prosjek(niz[i]) < prosjek(niz[worst]))
			worst = i;
	}
	cout << "ID:" << niz[worst].id << endl;
	cout << "Ime:" << niz[worst].ime << endl;
	cout << "Prezime:" << niz[worst].prezime << endl;
	cout << "Spol:" << niz[worst].spol << endl;
	cout << "Kviz 1:" << niz[worst].kviz_1 << endl;
	cout << "Kviz 2:" << niz[worst].kviz_2 << endl;
	cout << "Midterm:" << niz[worst].midterm << endl;
	cout << "Final:" << niz[worst].final << endl;
	cout << "Total:" << niz[worst].total << endl;
	cout << "----------------------------------" << endl;
	cout << endl;
}




int find(Student niz[], int size)
{
	if (!(size))
	{
		cout << "Nema studenata.";
		return NULL;
	}

	char id[10];
	cout << "Unesite ID studenta." << endl;
	std::cin >> id;

	int i;
	for (i = 0; i < size; i++)
	{
		if (!strcmp(id, niz[i].id))
		{
			cout << "Student pronadjen" << endl;
			return i;
		}
	}
	cout << "Nema studenta sa tim ID-om" << endl;
	return NULL;
}

void sort(Student niz[], int size)
{
	Student temp;
	int i;
	int j;
	for (i = 0; i < size; i++)
	{
		for (j = i; j < size; j++)
		{
			if (niz[i].total < niz[j].total)
			{
				temp = niz[i];
				niz[i] = niz[j];
				niz[j] = temp;
			}
		}
	}
}

void dodaj_studenta(Student niz[], int* size)
{
	char id[10];
	cout << "Unesite ID studenta." << endl;

	std::cin >> id;

	if (nadi(niz, *size, id) == -1)
	{
		cout << "Student s tim ID-om vec postoji!";
		return;
	}

	Student* novi;
	if (!(novi = (Student*)malloc(sizeof(Student))))
	{
		cout << "Greska.";
		return;
	}
	if (!(novi->id = (char*)malloc(sizeof(char) * 10)))
	{
		cout << "Greska.";
		return;
	}
	if (!(novi->ime = (char*)malloc(sizeof(char) * 30)))
	{
		cout << "Greska.";
		return;
	}
	if (!(novi->prezime = (char*)malloc(sizeof(char) * 30)))
	{
		cout << "Greska.";
		return;
	}
	strcpy_s(novi->id, 10, id);

	cout << "Unesite ime studenta." << endl;
	std::cin >> novi->ime;

	cout << "Unesite prezime studenta." << endl;
	std::cin >> novi->prezime;

	while ((novi->spol != 'm') && (novi->spol != 'z'))
	{
		cout << "Unesite spol studenta. (m/z)" << endl;
		std::cin >> novi->spol;
	}


	while (novi->kviz_1 > 5 || novi->kviz_1 < 0)
	{
		cout << "Unesite ocjenu studenta prvog kviza. (1-5)" << endl;
		std::cin >> novi->kviz_1;
	}

	while (novi->kviz_2 > 5 || novi->kviz_2 < 0)
	{
		cout << "Unesite ocjenu studenta drugog kviza. (1-5)" << endl;
		std::cin >> novi->kviz_2;
	}

	while (novi->midterm > 5 || novi->midterm < 0)
	{
		cout << "Unesite ocjenu studenta na sredini semestra. (1-5)" << endl;
		std::cin >> novi->midterm;
	}

	while (novi->final > 5 || novi->final < 0)
	{
		std::cout << "Unesite ocjenu studenta na kraju semestra. (1-5)" << std::endl;
		std::cin >> novi->final;
	}
	novi->total = novi->kviz_1 + novi->kviz_2 + novi->midterm + novi->final;
	niz[*size] = *novi;
	(*size) += 1;

}

void ukloni(Student niz[], int* size)
{
	if (!(*size))
	{
		cout << "Nema studenata.";
		return;
	}
	char id[10];
	cout << "Unesite tocan ID studenta kojeg zelite ukloniti.";
	std::cin >> id;
	int i;
	int j;
	while (1)
	{
		i = nadi(niz, *size, id);
		if (i >= 0)
		{
			free(niz[i].id);
			free(niz[i].ime);
			free(niz[i].prezime);
			for (j = i; j < (*size) - 1; j++)
			{
				niz[j] = niz[j + 1];
			}
			(*size) -= 1;
			return;
		}
		cout << "Unesite tocan ID studenta kojeg zelite ukloniti.";
		cin >> id;
	}
}
void a_id(Student niza, Student niz[], int size)
{
	char id[10];
	cout << "Unesite novi ID studenta." << endl;
	cin >> id;

	int i;
	i = nadi(niz, size, id);
	if (i >= 0)
	{
		cout << "Vec postoji student s tim ID-om";
		return;
	}
	strcpy_s(niza.id, 10, id);
}

void a_ime(Student niza)
{
	cout << "Unesite novo ime studenta." << endl;
	cin >> niza.ime;
	cout << "Unesite novo prezime studenta." << endl;
	cin >> niza.prezime;
}

void a_spol(Student niza)
{
	char spol;
	cout << "Unesite spol studenta." << endl;
	cin >> spol;
	if (!(spol == 'm' or spol == 'z'))
	{
		cout << "Greska u unosu" << endl;
		return;
	}
	niza.spol = spol;
}

void a_ocjene(Student niza)
{
	int opcija;
	while (1)
	{
		cout << "Koju ocjenu zelite azurirati?" << endl;
		cout << "1:Kviz 1   2:Kviz 2   3:Midterm  4:Final  5:Izlaz" << endl;
		cin >> opcija;
		switch (opcija) {
		case(1):
			cout << "Unesite novu ocjenu kviza." << endl;
			cin >> niza.kviz_1;
			break;
		case(2):
			cout << "Unesite novu ocjenu kviza." << endl;
			cin >> niza.kviz_2;
			break;
		case(3):
			cout << "Unesite novu ocjenu midterma." << endl;
			cin >> niza.midterm;
			break;
		case(4):
			cout << "Unesite novu finalnu ocjenu." << endl;
			cin >> niza.final;
			break;
		default:
			return;
		}
		niza.total = niza.kviz_1 + niza.kviz_2 + niza.midterm + niza.final;
	}
}

void azuriraj(Student niz[], int size) {
	if (!(size))
	{
		cout << "Nema studenata.";
		return;
	}
	char id[10];
	int i;
	int opcija;
	while (1)
	{
		cout << "Unesite ID studenta?" << endl;
		cin >> id;
		i = nadi(niz, size, id);
		if (i >= 0)
		{
			while (1)
			{
				cout << "�to �elite a�urirati?" << endl;
				cout << "Opcije :" << endl;
				cout << "1: ID  2: Ime  3:  Spol  4:  Ocjene  5: Izlaz" << endl;
				cin >> opcija;
				switch (opcija)
				{
				case 1:
					a_id(niz[i], niz, size);
					break;
				case 2:
					a_ime(niz[i]);
					break;
				case 3:
					a_spol(niz[i]);
					break;
				case 4:
					a_ocjene(niz[i]);
					break;
				default:
					return;
				}

			}
		}
		else
		{
			cout << "Unesite tocan ID studenta kojeg �elite a�urirati.";
			cin >> id;
		}

	}

}



int main()
{
	Student niz[200];
	int size = 0;

	while (1)
	{
		int opcija;

		cout << "Izbornik" << endl;
		cout << "1: Dodaj studenta   2: Ukloni studenta   3: Azuriraj studenta  4: Prikazi sve zapise " << endl;
		cout << endl;
		cout << "5:  Prosjek studenta  6: Najbolji student   7: Najgori student  8: Pronadji studenta " << endl;
		cout << endl;
		cout << "9: Sortiraj  10: Izlaz" << endl;

		cin >> opcija;


		switch (opcija) {
		case(1):
			dodaj_studenta(niz, &size);
			break;
		case(2):
			ukloni(niz, &size);
			break;
		case(3):
			azuriraj(niz, size);
			break;
		case(4):
			svi(niz, size);
			break;
		case(5):
			prosjek(niz, size);
			break;
		case(6):
			best(niz, size);
			break;
		case(7):
			worst(niz, size);
			break;
		case(8):
			find(niz, size);
			break;
		case(9):
			sort(niz, size);
			break;
		default:
			return 1;

		}
		std::cout << endl;
		std::cout << endl;
	}


}
