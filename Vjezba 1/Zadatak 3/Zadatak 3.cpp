
#include "pch.h"
#include <iostream>

int& fun(int niz[], int size)
{
	int i;
	for (i = 0; i < size; i++)
	{
		if ((niz[i] % 10 + (niz[i] / 100) % 10) == 5) return niz[i];
	}
}

int main()
{
	int niz[] = { 1234,1243,11 };
	int size = 3;
	fun(niz, size)++;
	int i;
	for (i = 0; i < size; i++)
	{
		std::cout << niz[i] << std::endl;
	}
	
}
