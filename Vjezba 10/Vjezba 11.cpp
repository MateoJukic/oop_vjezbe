
#include "pch.h"
#include <iostream>
#include <vector>
#include <fstream>
#include "except.h"

using namespace std;

char unos() {
	char a;
	std::cin >> a;
	return a;
}

char operacija() {
	char a;
	std::cin >> a;
	return a;
}

int rez(char broj1, char broj2, char operacija) {
	int b1 = broj1 - 48;
	int b2 = broj2 - 48;
	if (operacija == '+') {
		return b1 + b2;
	}
	if (operacija == '-') {
		return b1 - b2;
	}
	if (operacija == '*') {
		return b1 * b2;
	}
	if (operacija == '/') {
		return b1 / b2;
	}
}


int main()
{
	std::ofstream myfile;
	myfile.open("error.log", std::ios_base::out | std::ios_base::app);
	std::vector <int> v;
	int broj1;
	int broj2;
	except error;
	char op;
	while (true) {
		std::cout << "Unesite prvi broj!" << std::endl;
		broj1 = unos();
		std::cout << "Unesite drugi broj!" << std::endl;
		broj2 = unos();
		std::cout << "Unesite operaciju broj!" << std::endl;
		op = operacija();
		try {
			if (!isdigit(broj1) || !(isdigit(broj2))) {
				error.promjena("Unos nije broj!");
				throw error;
			}
			if (op != '+' && op != '-' && op != '*' && op != '/') {
				error.promjena("Unos nije operacija!");
				throw error;
				
			}
			if (broj2 == 0 && op == '/') {
				error.promjena("Dijeljenje sa nulom!");
				throw error;
			}
		}
		catch (except& o) { o.printerror(); continue;}
		std::cout << "Rezultat je: " << std::endl;
		std::cout << rez(broj1, broj2, op) << std::endl;
		v.push_back(rez(broj1, broj2, op));
	}
}


