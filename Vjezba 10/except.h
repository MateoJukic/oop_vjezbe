#pragma once
#include <string>
#include <fstream>
class except {
	std::string poruka;
public:
	except() { poruka = ""; }
	void promjena(std::string p) { poruka = p; }
	void printerror() {
		std::ofstream myfile;
		myfile.open("error.log", std::ios_base::out | std::ios_base::app);
		myfile << poruka << std::endl;
	}
 };

