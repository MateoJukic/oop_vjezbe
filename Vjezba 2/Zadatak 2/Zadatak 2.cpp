

#include "pch.h"
#include <iostream>

using namespace std;

void sort(int* niz, int l, int r) {

	int i = l;
	int j = r;

	int temp;

	int pivot = niz[(l + r) / 2];

	while (i <= j) {

		while (niz[i] < pivot) i++;

		while (niz[j] > pivot) j--;

		if (i <= j) {

			temp = niz[i];
			niz[i] = niz[j];
			niz[j] = temp;

			i++;
			j--;

		}

	}
	if (l < j) sort(niz, l, j);

	if (i < r) sort(niz, i, r);
}

void odvoji(int niz[], int size) {

	sort(niz, 0, size-1);
	int i;
	int n=0;
	int p=0;
	for (i = 0; i < size; i++) {
		if (niz[i] % 2) {
			n++;
		}
		else {
			p++;
		}
	}

	int* parni = new int[p];
	int* neparni = new int[n];

	n = 0;
	p = 0;
	for (i = 0; i < size; i++) {
		if (niz[i] % 2) {
			neparni[n] = niz[i];
			n++;
		}
		else {
			parni[p] = niz[i];
			p++;
		}
	}


	for (i = 0; i < size; i++) {
		if (i < p) niz[i] = parni[i];
		else niz[i] = neparni[i-p];
	}
	delete[] parni;
	delete[] neparni;
}

int main()
{
	int niz[] = {1,2,3,4,5,6,7,8,9,10};
	int size = sizeof(niz) / 4;
	odvoji(niz, size);
	int i;
	for (i = 0; i < size; i++) {

		cout << niz[i]<<endl;

	}
}
