

#include "pch.h"
#include <iostream>

struct vektor {
	int* niz;
	int logic;
	int fizi;
};

vektor* vektor_new() {
	vektor* novi;
	novi = new vektor;
	int n=0;
	
	std::cout << "Unesite velicinu novog vektora." << std::endl;
	std::cin >> n;
	novi->niz = new int[n];

	novi->fizi = n;
	novi->logic = 0;

	return novi;
}

void vektor_delete(vektor *v) {
	delete[] v->niz;
	delete v;
	v = NULL;
}

void niz_copy(int* niz, int size, int* novi) {
	int i;
	for (i = 0; i < size; i++) {
		novi[i] = niz[i];
	}
}

void vektor_push_back(vektor* v,int broj) {

	if ((v->logic + 1) > v->fizi) {

		int* niz;
		niz = new int[v->fizi * 2];
		v->fizi *= 2;

		niz_copy(v->niz, v->logic, niz);
		
		delete[] v->niz;
		v->niz = niz;
	}
	v->niz[v->logic] = broj;
	v->logic += 1;
}

void vektor_pop_back(vektor v) {
	v.logic -= 1;
}

int& vektor_front(vektor v) {
	return v.niz[0];
}

int& vektor_back(vektor v) {
	return v.niz[v.logic - 1];
}

int vektor_size(vektor v) {
	return v.logic;
}

int main()
{
	vektor* v;
	v = vektor_new();
	vektor_push_back(v, 2);
	vektor_push_back(v, 3);
	vektor_push_back(v, 4);
	std::cout <<"Prvi clan:"<< vektor_front(*v) << std::endl;
	std::cout <<"Zadnji clan:"<< vektor_back(*v) << std::endl;
	std::cout <<"Velicina vektora:"<< vektor_size(*v) << std::endl;
	vektor_pop_back(*v);
	std::cout <<"Zadnji clan:"<< vektor_back(*v) << std::endl;
	vektor_delete(v);


}

