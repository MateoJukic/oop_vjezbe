#include "pch.h"
#include <iostream>
#include <vector>
#include <time.h>

using namespace std;

void igra() {
	
	vector <int> sibice(21);

	int potez = 1;
	int i;
	int j;

	while (sibice.size() >= 2) {

		if (potez) {
			cout << "Na redu :  Igrac"<< endl;
			cout << "Preostalo je " << sibice.size() << " sibica/e" << endl;
			cout << "Koliko sibica zelite uzeti?" << endl;
			cin >> i;
			cout << "Igrac uzima " << i << " sibica/e" << endl;
			for (j = 0; j < i; j++) {
				sibice.pop_back();
			}
			potez--;
		}
		else {
			cout << "Na redu :  racunalo"<< endl;
			cout << "Preostalo je " << sibice.size() << " sibica/e" << endl;
			i = rand() % 3 + 1;
			cout << "Racunalo uzima " << i << " sibica/e" << endl;
			for (j = 0; j < i; j++) {
				sibice.pop_back();
			}
			potez++;
		}

	}
	if (potez) cout << "GUBITAK."<<endl;
	else cout << "POBJEDA!"<<endl;

}

int main()
{
	srand(time(NULL));
	char izbor = 'y';
	while (izbor == 'y') {
		igra();
		cout << "Nova igra?   y/n"<<endl;
		cin >> izbor;
	}
}
