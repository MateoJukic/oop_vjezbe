#include "pch.h"
#include <iostream>
#include <string>
#include <vector>
using namespace std;

struct Producent {
	string name;
	string movie;
	int year;

};

void unos(vector <Producent> &p) {

	cout << "Koliko producenata zelite unijeti?" << endl;
	Producent pp;
	int n;
	cin >> n;
	int i;
	for (i = 0; i < n; i++) {
		cout << "Unesite Ime Producenta." << endl;
		cin >> pp.name;
		cout << endl << endl;
		cout << "Unesite Ime filma." << endl;
		cin >> pp.movie;
		cout << endl << endl;
		cout << "Unesite godinu izlaska filma." << endl;
		cin >> pp.year;
		cout << endl << endl;
		p.push_back(pp);
	}

}

void popular(vector <Producent> p) {
	vector <string> popularni;
	vector <int> popularnost;
	if (!p.size()) return;

	unsigned brojac = 1;

	unsigned i;
	unsigned j;
	unsigned found = 0;
	unsigned max = 0;
	for (i = 0; i < p.size(); i++) {

		j = i;
		
		while(j>0) {
			if (!p[i].name.compare(p[j - 1].name)) {
				found = 1;
				break;
			}
			j--;
		}
		if (found) {
			found = 0;
			continue;
		}
		for (j = i+1; j < p.size()-1; j++) {

			if (!p[i].name.compare(p[j].name)) {
				brojac++;
			}
		}
		popularnost.push_back(brojac);
	}

	for (i=0; i < popularnost.size(); i++) {
	
		if (popularnost[max] < popularnost[i])max = i;
	}
	popularni.push_back(p[max].name);

	for (i=0; i < popularnost.size(); i++) {
		if ((popularnost[max] == popularnost[i]) && max != i) popularni.push_back(p[i].name);
	}
	cout << "Najzastupljeniji producenti su: " << endl;
	for (i = 0; i < popularni.size(); i++) {
		cout << popularni[i] << endl;
	}

	

}

int main(){
	vector <Producent> producenti;
	unos(producenti);
	popular(producenti);




	
}
