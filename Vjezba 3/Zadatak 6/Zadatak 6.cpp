#include "pch.h"
#include <iostream>
#include <string>
#include <vector>
using namespace std;

int provjera(string s) {
	if (s.size() > 20) return 0;
	unsigned i;
	for (i = 0; i < s.size(); i++) {
		if (!isalnum(s[i])) return 0;
	}

	for (i = 0; i < s.size(); i++) {
		if(isdigit(s[i])) return 2;
		if (!isupper(s[i])) return 0;
	}
	return 3;
}

void transformb(string &s) {

	unsigned i;
	unsigned j;

	unsigned brojac = 1;

	for (i = 0; i < s.size(); i++) {
		j = i + 1;
		if (s[i] == s[j]) {
			while (s[i] == s[j]) {
				brojac++;
				j++;
			}
		
			s[i] = brojac + '0';

			if(brojac > 2) s.erase(i + 1, brojac - 2);
			
			brojac = 1;
		}
	}
}

void transforms(string &s) {

	unsigned i;
	unsigned j;
	unsigned range;

	unsigned brojac;

	for (i = 0; i < s.size(); i++) {
		if (isdigit(s[i])) {
			brojac = s[i] - '0';
			s[i] = s[i + 1];
			range = i + brojac;
			while (i < range - 2) {
				s.push_back(s[i + 1]);
				for (j = s.size() - 1; j > i; j--) {
					swap(s[j], s[j - 1]);
				}
				i++;
			}

		}
	}
}

void stringovi() {
	cout << "Unesite broj stringova." << endl;
	vector <string> v;
	string s;
	unsigned N;
	cin >> N;
	unsigned i;
	for (i = 0; i < N; i++) {
		cout << "Unesite pravilan string." << endl;
		cin >> s;
		if (!provjera(s)) {
			i--;
			continue;
		}
		v.push_back(s);
	}
	for (i = 0; i < v.size(); i++) {
		if (provjera(v[i]) == 2) {
			transforms(v[i]);
		}
		else {
			transformb(v[i]);
		}
	}
	cout << endl << endl;
	for (i = 0; i < v.size(); i++) {
		cout << v[i] << endl;
	}

}

int main()
{
	stringovi();
}
