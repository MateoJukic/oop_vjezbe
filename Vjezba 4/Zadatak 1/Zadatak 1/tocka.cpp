#include "pch.h"
#include "tocka.h"
#include <random>
#include <iostream>


void tocka::set_tocka(int x, int y, int z) {
	_x = x; 
	_y = y;
	_z = z;
}
void tocka::set_tocka_random(int begin, int end) {
	_x = rand() % (abs(end - begin) + 1) + begin;
	_y = rand() % (abs(end - begin) + 1) + begin;
	_z = rand() % (abs(end - begin) + 1) + begin;
}

int tocka::get_duzina() const{
	return _z; 
}

int tocka::get_sirina() const{
	return _x;
}

int tocka::get_visina() const{
	return _y;
}

int tocka::daljina2d(tocka t) const{
	return round(sqrt((_x - t._x) * (_x - t._x) + (_y - t._y) * (_y - t._y)));
}

int tocka::daljina3d(tocka t) const{
	return round(sqrt((_x - t._x) * (_x - t._x) + (_y - t._y) * (_y - t._y) + (_z - t._z) * (_z - t._z)));
}

void tocka::print() const{
	std::cout << "x = " << _x << " y = " << _y << " z = " << _z << std::endl;
}