#ifndef TOCKA_H
#define TOCKA_H

class tocka {
	int _x;
	int _y;
	int _z;
public:
	void set_tocka(int x = 0, int y = 0, int z = 0);
	void set_tocka_random(int begin, int end);
	int get_duzina() const;
	int get_sirina() const;
	int get_visina() const;

	int daljina2d(tocka t) const;
	int daljina3d(tocka t) const;
	void print() const;
};
#endif