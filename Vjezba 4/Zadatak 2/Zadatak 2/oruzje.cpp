#include "pch.h"
#include <iostream>
#include "oruzje.h"

void oruzje::set(tocka b, int metak, int kapacitet) {
	_poz = b;
	_metak = metak;
	_kapacitet = kapacitet;
}

void oruzje::print() const{
	std::cout << "Pozicija: ";
	_poz.print();
	std::cout << "Oruzje ima " << _metak << " metka/metaka od " << _kapacitet << std::endl;
}

void oruzje::shoot() {
	if (_metak) {
		_metak--;
	}
	else std::cout << "Oruzje nema metaka." << std::endl;
}

void oruzje::reload() {
	_metak = _kapacitet;
}

int oruzje::get_metak() const{
	return _metak;
};