#ifndef ORUZJE_H
#define ORUZJE_H
#include "../../Zadatak 1/Zadatak 1/tocka.h"

class oruzje {
	tocka _poz;
	int _metak;
	int _kapacitet;
public:
	void set(tocka b, int metak, int kapacitet);
	void print() const;
	void shoot();
	void reload();
	int get_metak() const;

};




#endif
