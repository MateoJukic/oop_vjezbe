#include "pch.h"
#include "meta.h"
#include <iostream>
#include <random>

void meta::set_meta(tocka c, int s, int v) {
	_pocetna = c;
	_sirina = s;
	_visina = v;
}

void meta::set_random(int donja, int gornja) {
	_pocetna.set_tocka_random((rand() % donja) + 1,(rand() % gornja) + 1);
	_sirina = rand() % (abs(gornja - donja) + 1) + donja;
	_visina = rand() % (abs(gornja - donja) + 1) + donja;
}

void meta::print() const {
	_pocetna.print();
	std::cout << "Visina = "<<_visina << std::endl;
	std::cout << "Sirina = "<<_sirina << std::endl;
	std::cout <<"Pogodjena = "<< _hit << std::endl;
}

int meta::get_v() const {
	return _visina;
}

tocka meta::get_tocka() const {
	return _pocetna;
}

void meta::hit(){
	_hit = 1;
}
