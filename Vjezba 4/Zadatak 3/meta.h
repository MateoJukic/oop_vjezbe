#ifndef META_H
#define META_H
#include "../Zadatak 1/Zadatak 1/tocka.h"

class meta {
	tocka _pocetna;
	int _sirina;
	int _visina;
	bool _hit = false;
public:
	void set_meta(tocka c, int s, int v);
	void set_random(int donja, int gornja);
	void print() const;
	int get_v() const;
	void hit();
	tocka get_tocka() const;
};

#endif
