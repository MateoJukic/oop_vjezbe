#include "pch.h"
#include <iostream>
#include "ZooAnimal.h"
#include <vector>
#include <string>

using namespace std;

void pothranjenost(vector <ZooAnimal> &v) {

	int i;
	for (i = 0; i < v.size(); ++i) {
		if (v[i].fat()) {
			v[i].obrok(0);
		}
		else {
			v[i].obrok(1);
		}
	}
}

int main()
{
	
	string vrsta;
	string ime;
	int godina;
	int kavezi;
	int obroci;
	int vijek;
	int n;
	cout << "Unesite broj zivotinja." << endl;
	cin >> n;
	int i;
	int j;
	vector <ZooAnimal> v;
	for (i=0; i < n; ++i) {
		cout << "Unesite vrstu zivotinje." << endl;
		cin >> vrsta;
		cout << "Unesite ime zivotinje." << endl;
		cin >> ime;
		cout << "Unesite starost zivotinje." << endl;
		cin >> godina;
		cout << "Unesite broj kaveza zivotinje." << endl;
		cin >> kavezi;
		cout << "Unesite broj obroka zivotinje." << endl;
		cin >> obroci;
		cout << "Unesite zivotni vijek zivotinje." << endl;
		cin >> vijek;
		ZooAnimal temp(vrsta, ime, godina, kavezi, obroci, vijek);
		cout << "Unesite broj prijasnjih vaganja." << endl;
		cin >> godina;
		temp.broj_vaganja = godina;
		for (j = 0; j < godina; ++j) {
			cout << "Unesite masu vaganja." << endl;
			cin >> kavezi;
			cout << "Unesite godinu vaganja." << endl;
			cin >> vijek;
			temp.masa[j].masa = kavezi;
			temp.masa[j].godina = vijek;
		}
		v.push_back(temp);
	}
	for (i = 0; i < n; ++i) {
		cout << "Prije:     " << endl << endl;

		v[i].print();
		pothranjenost(v);
		cout << "Poslije:     " << endl << endl << endl;

		v[i].print();
	}
	v[0].~ZooAnimal();
	v[0].print();


}

