#include "pch.h"
#include "ZooAnimal.h"
#include <iostream>
#include <string>
#include <ctime>

ZooAnimal::ZooAnimal(std::string vrst, std::string im, int godin, int kavez, int obroc, int vije) {
	vrsta = vrst;
	ime = im;
	godina_rodjenja = godin;
	kavezi = kavez;
	obroci = obroc;
	vijek = vije;
	masa = new mass[2 * vijek];
}

ZooAnimal::~ZooAnimal() {
	delete[] masa;
}

ZooAnimal::ZooAnimal(const ZooAnimal& other) {
	vrsta = other.vrsta;
	ime = other.ime;
	godina_rodjenja = other.godina_rodjenja;
	kavezi = other.kavezi;
	obroci = other.obroci;
	vijek = other.vijek;
	int i;
	masa = new mass[2 * vijek];
	broj_vaganja = other.broj_vaganja;
	for (i = 0; i < broj_vaganja; i++) {
		masa[i] = other.masa[i];
	}
}

void ZooAnimal::obrok(int i) {
	if (i) {
		obroci++;
	}
	else {
		obroci--;
	}
}

void ZooAnimal::vaganje(int m, int v) {

	time_t t = time(0);
	struct tm now;
	localtime_s(&now,&t);
	int god = now.tm_year + 1900;
	if (god != v) {
		std::cout << "Nije tekuca godina"<<std::endl;
		return;
	}
	int i;
	bool flag = 1;
	for (i = 0; i < broj_vaganja; i++) {
		if (masa[i].godina == v) {
			masa[i].masa = m;
			flag = 0;
		}
	}
	if (flag) {
		masa[broj_vaganja].masa = m;
		masa[broj_vaganja].godina = v;
		broj_vaganja++;
	}
}

bool ZooAnimal::fat() const{
	time_t t = time(0);
	struct tm now;
	localtime_s(&now, &t);
	int god = now.tm_year + 1900;
	if (broj_vaganja < 2 || masa[broj_vaganja-1].godina != god || masa[broj_vaganja - 2].godina != god-1) {
		std::cout << "Nedovoljno podataka" << std::endl;
		return false;
	}

	if ((double)masa[broj_vaganja-1].masa*1.1 >= (double)masa[broj_vaganja-2].masa) {
		return true;

	}
	else {
		return false;
	}
}

void ZooAnimal::print() const {
	std::cout << "Vrsta: " << vrsta << std::endl;
	std::cout << "Ime: " << ime << std::endl;
	std::cout << "Godina rodjenja: " << godina_rodjenja << std::endl;
	std::cout << "Broj kaveza: " << kavezi << std::endl;
	std::cout << "Broj obroka: " << obroci << std::endl;
	std::cout << "Zivotni vijek: " << vijek << std::endl;

	int i;
	for (i = 0; i < broj_vaganja; i++) {
		std::cout << "Masa: " << masa[i].masa << std::endl;
		std::cout << "Godina: " << masa[i].masa << std::endl;
	}
}	
