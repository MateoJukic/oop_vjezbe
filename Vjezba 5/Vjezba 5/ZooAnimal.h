#ifndef ZIVOTINJA_H
#define ZIVOTINJA_H
#include <iostream>

class mass {
	
public:
	int masa;
	int godina;
};

class ZooAnimal {
	std::string vrsta;
	std::string ime;
	int godina_rodjenja;
	int kavezi;
	int obroci;
	int vijek;

public:
	int broj_vaganja;
	mass* masa;
	ZooAnimal();
	ZooAnimal(std::string vrsta, std::string ime, int godina_rodjenja, int kavezi, int obroci, int vijek);
	~ZooAnimal();
	ZooAnimal(const ZooAnimal& other);
	void vaganje(int m, int v);
	bool fat() const;
	void obrok(int i);
	void print() const;
};




#endif
