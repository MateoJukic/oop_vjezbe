

#include "pch.h"
#include <iostream>
#include "array.h"
using namespace OOP;

int main()
{
	cout << "Unesite duljinu prvog niza" << endl;
	int n;
	cin >> n;

	Array niz1(n);

	cout << "Unesite duljinu drugog niza" << endl;
	cin >> n;
	Array niz2(n);

	cin >> niz1;
	cout<< niz1;

	cin >> niz2;
	cout << niz2;
	cout << "Kopija drugog niza:" << endl;
	Array niz3(1);
	niz3 = niz2;
	cout << niz3;
	cout << endl;
	niz3 = niz1 + niz2;
	cout << "Prvi niz + Drugi niz:" << endl;
	cout << niz3;
	cout << endl;
	cout << "Presjek niz1 i niz2 :" << endl;
	niz3 = niz1 - niz2;
	cout << niz3;
	cout << endl;
	cout << "Jesu li nizovi ekvivalentni?" << endl;
	cout << (niz1 == niz2) << endl;
	cout << endl;
	cout << "Jesu li nizovi disjunktni?" << endl;
	cout << (niz1 != niz2) << endl;
	cout << endl;
	cout << "Prvi element prvog niza je:" << endl;
	cout<<niz1[0] << endl;
	cout << endl;
	niz1[0] = 3;
	cout << "Prvi element  nakon promjene je:" << endl;
	cout << niz1[0]<< endl;
	cout << endl;
	cout << "Broj razlicitih objekata koji su koristeni:" << endl;
	cout<<niz1.clan();

}

