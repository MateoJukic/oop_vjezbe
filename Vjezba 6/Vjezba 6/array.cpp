#include "pch.h"
#include "Array.h"
using namespace OOP;

int  Array::counter = 0;


Array::Array(int s) {
	size = s;
	counter++;
	niz = new int[s];
}

Array::~Array() {
	delete[] niz;
}

Array::Array(Array &other) {
	int i;
	this->size = other.size;
	this->niz = new int[other.size];
	for (i = 0; i < other.size; ++i) {
		this->niz[i] = other.niz[i];
	}
}


namespace OOP {
	ostream & operator << (ostream& out, const Array &a) {
		int i;
		for (i = 0; i < a.size; ++i) {
			out << a.niz[i];
		}
		out << endl;
		return out;
	}

	istream& operator >> (istream &in, Array &a) {
		int i;
		cout << "Unesite clanove niza." << endl;
		for (i = 0; i < a.size; ++i) {
			in >> a.niz[i];
		}
		return in;
	}
}

void Array::operator = (const Array &other) {
	int i;
	this->size = other.size;
	this->niz = new int[other.size];
	for (i = 0; i < other.size; ++i) {
		this->niz[i] = other.niz[i];
	}
}

Array Array::operator + (const Array &other) const{
	int i;
	int novi_size = this->size + other.size;
	Array novi(novi_size);
	for (i = 0; i < this->size; ++i) {
		novi.niz[i] = this->niz[i];
	}
	for (i = 0; i < other.size; ++i) {
		novi.niz[this->size + i] = other.niz[i];
	}
	novi.size = novi_size;
	return novi;
}

Array Array::operator - (const Array &other)const {
	int i;
	int j;
	int brojac = 0;
	for (i = 0; i < this->size; ++i) {
		for (j = 0; j < other.size; ++j) {
			if (this->niz[i] == other.niz[j]) {
				brojac++;
			}
		}
	}
	if (!brojac) {
		return NULL;
	}
	Array novi(brojac);
	int k = 0;

	for (i = 0; i < this->size; ++i) {
		for (j = 0; j < other.size; ++j) {
			if (this->niz[i] == other.niz[j]) {
				novi.niz[k] = niz[i];
				k++;
			}
		}
	}

	novi.size = brojac;
	return novi;
}

bool Array::operator == (const Array &other)const {
	int i;
	int j;
	for (i = 0; i < this->size; ++i) {
		for (j = 0; j < other.size; ++j) {
			if (this->niz[i] != other.niz[j]) {
				return false;
			}
		}
	}
	return true;
}

bool Array::operator != (const Array &other)const {
	int i;
	int j;
	for (i = 0; i < this->size; ++i) {
		for (j = 0; j < other.size; ++j) {
			if (this->niz[i] == other.niz[j]) {
				return false;
			}
		}
	}
	return true;
}

int& Array::operator [] (const int indeks)const {
	if (indeks < size && indeks >= 0) {
		return niz[indeks];
	}
	else return niz[0];
	
}
int Array::clan() {
	return counter;
}