#ifndef ARRAY_H
#define ARRAY_H
#include <iostream>
using namespace std;

namespace OOP {
	class Array {
		int* niz;
		int size;
		static int counter;
	public:
		Array(int size);
		~Array();
		Array(Array& other);
		friend ostream& operator << (ostream& out, const Array &a);
		friend istream& operator >> (istream &in, Array &a);
		void operator = ( const Array &other);
		Array operator + (const Array &other)const;
		Array operator - (const Array &other)const;
		bool operator == (const Array &other)const;
		bool operator != (const Array &other)const;
		int& operator [] (const int indeks)const;
		int clan();
	};
}
#endif 
