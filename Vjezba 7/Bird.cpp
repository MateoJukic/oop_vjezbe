#include "pch.h"
#include "Bird.h"

using namespace std;


Bird::Bird(std::string vrsta, std::string ime, int godina_rodjenja, int kavezi, int obroci, int vijek, int period, int temp, std::string repro) : ZooAnimal(vrsta, ime, godina_rodjenja, kavezi, obroci, vijek) {

	IncubationPeriod = period;
	AvgTemp = temp;
	NacinRepro = repro;
}

std::istream& operator >> (std::istream& input, Bird& mimi) {
	cout << "Unesite inkubacijski period ptice." << endl;
	input >> mimi.IncubationPeriod;
	cout << "Unesite prosjecnu temperaturu tijela ptice." << endl;
	input >> mimi.AvgTemp;
	cout << "Unesite nacin reprodukcije ptice." << endl;
	input >> mimi.NacinRepro;
	return input;
}

std::ostream& operator << (std::ostream& output, Bird& mimi) {
	output << "Ime: " << mimi.ime << endl;
	output << "Vrsta: " << mimi.vrsta << endl;
	output << "Godina rodjenja: " << mimi.godina_rodjenja << endl;
	output << "Broj kaveza: " << mimi.kavezi << endl;
	output << "Broj obroka: " << mimi.obroci << endl;
	output << "Zivotni vijek: " << mimi.vijek << endl;
	output << "Inkubacijski period: " << mimi.IncubationPeriod << endl;
	output << "Prosjecna temperatura: " << mimi.AvgTemp << endl;
	output << "Nacin reprodukcije: " << mimi.NacinRepro << endl;
	return output;
}