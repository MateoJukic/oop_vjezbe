#ifndef BIRD_H
#define BIRD_H
#include "ZooAnimal.h"
#include <iostream>
class Bird : public ZooAnimal {
protected:
	int IncubationPeriod;
	int AvgTemp;
	std::string NacinRepro;
public:
	Bird(std::string vrsta, std::string ime, int godina_rodjenja, int kavezi, int obroci, int vijek, int period, int temp, std::string repro);
	friend std::istream& operator >> (std::istream& input, Bird& mimi);
	friend std::ostream& operator << (std::ostream& output, Bird& mimi);
};

#endif
