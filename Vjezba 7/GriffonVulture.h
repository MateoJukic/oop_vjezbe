#ifndef GRIFFONVULTURE_H
#define GRIFFONVULTURE_H
#include "Bird.h"


class GriffonVulture : public Bird {
	int kolicina;
public:
	GriffonVulture(std::string vrsta, std::string ime, int godina_rodjenja, int kavezi, int obroci, int vijek, int period, int temp, std::string repro, int kolicina) : Bird(vrsta, ime, godina_rodjenja, kavezi, obroci, vijek, period, temp, repro) { this->kolicina = kolicina; };

};


#endif