#include "pch.h"
#include "Mammal.h"
using namespace std;

Mammal::Mammal(std::string vrsta, std::string ime, int godina_rodjenja, int kavezi, int obroci, int vijek, int period, int temp, std::string repro) : ZooAnimal(vrsta, ime, godina_rodjenja, kavezi, obroci, vijek) {

	GestationPeriod = period;
	AvgTemp = temp;
	NacinRepro = repro;
}

std::istream& operator >> (std::istream& input, Mammal& mimi) {
	cout << "Unesite gestacijski period sisavca." << endl;
	input >> mimi.GestationPeriod;
	cout << "Unesite prosjecnu temperaturu tijela sisavca." << endl;
	input >> mimi.AvgTemp;
	cout << "Unesite nacin reprodukcije sisavca." << endl;
	input >> mimi.NacinRepro;
	return input;
}

std::ostream& operator << (std::ostream& output, Mammal& mimi) {
	output << "Ime: " << mimi.ime << endl;
	output << "Vrsta: " << mimi.vrsta << endl;
	output << "Godina rodjenja: " << mimi.godina_rodjenja << endl;
	output << "Broj kaveza: " << mimi.kavezi << endl;
	output << "Broj obroka: " << mimi.obroci << endl;
	output << "Zivotni vijek: " << mimi.vijek << endl;
	output << "Gestacijski period: " << mimi.GestationPeriod << endl;
	output << "Prosjecna temperatura: " << mimi.AvgTemp << endl;
	output << "Nacin reprodukcije: " << mimi.NacinRepro << endl;
	return output;
}