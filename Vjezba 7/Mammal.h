#ifndef MAMMAL_H
#define MAMMAL_H
#include "ZooAnimal.h"
#include <iostream>

class Mammal : public ZooAnimal {
protected:
	int GestationPeriod;
	int AvgTemp;
	std::string NacinRepro;
public:
	Mammal(std::string vrsta, std::string ime, int godina_rodjenja, int kavezi, int obroci, int vijek, int period, int temp, std::string repro);
	friend std::istream& operator >> (std::istream& input, Mammal& mimi);
	friend std::ostream& operator << (std::ostream& output, Mammal& mimi);
};

#endif

