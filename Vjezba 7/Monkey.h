#ifndef MONKEY_H
#define MONKEY_H
#include "Mammal.h"

class Monkey : public Mammal {
	int kolicina;
public:
	Monkey(std::string vrsta, std::string ime, int godina_rodjenja, int kavezi, int obroci, int vijek, int period, int temp, std::string repro, int kolicina) : Mammal(vrsta, ime, godina_rodjenja, kavezi, obroci, vijek, period, temp, repro) { this->kolicina = kolicina; };
};


#endif
