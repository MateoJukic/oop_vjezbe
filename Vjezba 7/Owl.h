#ifndef OWL_H
#define OWL_H
#include "Bird.h"

class Owl : public Bird {
	int kolicina;
public:
	Owl(std::string vrsta, std::string ime, int godina_rodjenja, int kavezi, int obroci, int vijek, int period, int temp, std::string repro, int kolicina) : Bird(vrsta, ime, godina_rodjenja, kavezi, obroci, vijek, period, temp, repro) { this->kolicina = kolicina; };
};


#endif