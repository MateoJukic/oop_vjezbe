#include "pch.h"
#include "Reptile.h"

using namespace std;


Reptile::Reptile(std::string vrsta, std::string ime, int godina_rodjenja, int kavezi, int obroci, int vijek, int period, int temp, std::string repro) : ZooAnimal(vrsta, ime, godina_rodjenja, kavezi, obroci, vijek) {

	IncubationPeriod = period;
	LocalTemp = temp;
	NacinRepro = repro;
}

std::istream& operator >> (std::istream& input,Reptile& mimi) {
	cout << "Unesite inkubacijski period gmaza." << endl;
	input >> mimi.IncubationPeriod;
	cout << "Unesite temperaturu okoline gmaza." << endl;
	input >> mimi.LocalTemp;
	cout << "Unesite nacin reprodukcije gmaza." << endl;
	input >> mimi.NacinRepro;
	return input;
}

std::ostream& operator << (std::ostream& output, Reptile& mimi) {
	output << "Ime: " << mimi.ime << endl;
	output << "Vrsta: " << mimi.vrsta << endl;
	output << "Godina rodjenja: " << mimi.godina_rodjenja << endl;
	output << "Broj kaveza: " << mimi.kavezi << endl;
	output << "Broj obroka: " << mimi.obroci << endl;
	output << "Zivotni vijek: " << mimi.vijek << endl;
	output << "Inkubacijski period: " << mimi.IncubationPeriod << endl;
	output << "Prosjecna temperatura: " << mimi.LocalTemp << endl;
	output << "Nacin reprodukcije: " << mimi.NacinRepro << endl;
	return output;
}