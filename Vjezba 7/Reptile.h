#ifndef REPTILE_H
#define REPTILE_H
#include "ZooAnimal.h"
#include <iostream>

class Reptile : public ZooAnimal {
protected:
	int IncubationPeriod;
	int LocalTemp;
	std::string NacinRepro;
public:
	Reptile(std::string vrsta, std::string ime, int godina_rodjenja, int kavezi, int obroci, int vijek, int period, int temp, std::string repro);
	friend std::istream& operator >> (std::istream& input, Reptile& mimi);
	friend std::ostream& operator << (std::ostream& output, Reptile& mimi);


};

#endif