#ifndef TIGER_H
#define TIGER_H
#include "Mammal.h"

class Tiger : public Mammal {
	int kolicina;
public:
	Tiger(std::string vrsta, std::string ime, int godina_rodjenja, int kavezi, int obroci, int vijek, int period, int temp, std::string repro, int kolicina) : Mammal(vrsta, ime, godina_rodjenja, kavezi, obroci, vijek, period, temp, repro) { this->kolicina = kolicina; };
};


#endif