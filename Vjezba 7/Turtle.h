#ifndef TURTLE_H
#define TURTLE_H
#include "Reptile.h"

class Turtle :  public Reptile {
	int kolicina;
public:
	Turtle(std::string vrsta, std::string ime, int godina_rodjenja, int kavezi, int obroci, int vijek, int period, int temp, std::string repro, int kolicina) : Reptile(vrsta, ime, godina_rodjenja, kavezi, obroci, vijek, period, temp, repro) { this->kolicina = kolicina; };

};


#endif
