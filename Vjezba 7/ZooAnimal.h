#ifndef ZIVOTINJA_H
#define ZIVOTINJA_H
#include <iostream>
#include <time.h>
#include <string>

class mass {
	
public:
	int masa;
	int godina;
	mass() {
		masa = 0;
		godina = 2019;
	}
	mass(int m, int g) { masa = m; godina = g; }
};

class ZooAnimal {
protected:
	std::string vrsta;
	std::string ime;
	int godina_rodjenja;
	int kavezi;
	int obroci;
	int vijek;
	mass* masa;
	int broj_vaganja;

public:

	ZooAnimal() {
		
	}

	ZooAnimal(std::string vrsta, std::string ime, int godina_rodjenja, int kavezi, int obroci, int vijek);
	ZooAnimal(const ZooAnimal& other);
	//void vaganje(int m, int v);
	bool fat() const;
	void obrok(int i);
	void print() const;
	~ZooAnimal();
};




#endif
