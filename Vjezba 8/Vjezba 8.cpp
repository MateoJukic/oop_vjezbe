#include "bike.h"
#include "car.h"
#include "catamaran.h"
#include "ferry.h"
#include "seaplane.h"
#include "counter.h"
#include <chrono>
#include <thread>


int main(void) {
	using namespace oop;
	counter c;
	vehicle* v[] = { new bike, new car, new catamaran(30), new ferry(10, 5, 3), new seaplane(15) };
	size_t sz = sizeof v / sizeof v[0];
	for (unsigned i = 0; i < sz; ++i)
		c.add(v[i]);
	std::cout << "ukupno " << c.total() << " putnika" << std::endl;
	for (unsigned i = 0; i < sz; ++i)
		delete v[i];	using namespace std::this_thread; // sleep_for, sleep_until
	using namespace std::chrono; // nanoseconds, system_clock, seconds
	//projekt nije windows console application nego empty project	sleep_for(nanoseconds(5000000000));
}