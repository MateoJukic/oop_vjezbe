#pragma once
#include "vehicle.h"
namespace oop {
	class aircraft : virtual public vehicle {
	protected:
		string tip;
	public:
		aircraft() {
			tip = "air";
		}
		string type() { return tip; }
	};
}