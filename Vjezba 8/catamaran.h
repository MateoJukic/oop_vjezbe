#pragma once
#include "watercraft.h"
namespace oop {
	class catamaran :public watercraft {
	protected:
		unsigned putnici;
	public:
		catamaran(unsigned putnik) { putnici = putnik; }
		unsigned passengers() {
			return putnici;
		}

	};
}