#pragma once
#include "bike.h"
#include "car.h"
#include "ferry.h"
#include "catamaran.h"
#include "seaplane.h"
namespace oop {
	class counter {
		unsigned ukupno;
	public:
		counter() { ukupno = 0; }
		 void add(vehicle* v) { 
			 std::cout << "tip: " << v->type() << ", " << "putnika: " << v->passengers() << std::endl;
			ukupno += v->passengers();
		}
		unsigned total() { return ukupno; }
	};
}