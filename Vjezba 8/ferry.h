#pragma once
#include "watercraft.h"
namespace oop {
	class ferry :public watercraft {
	protected:
		unsigned putnici;
		unsigned auti;
		unsigned bicikli;
	public:
		ferry(unsigned putnik, unsigned auti, unsigned bicikli) { putnici = putnik; this->auti = auti; this->bicikli = bicikli; }
		unsigned passengers() {
			return putnici;
		}

	};
}