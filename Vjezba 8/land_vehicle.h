#pragma once
#include "vehicle.h"
namespace oop {
	class land_vehicle : virtual public vehicle {
	protected:
		string tip;
	public:
		land_vehicle() { tip = "land"; }
		string type() {
			return tip;
		}

	};
}