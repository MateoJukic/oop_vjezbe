#pragma once
#include "watercraft.h"
#include "aircraft.h"


namespace oop {
	class seaplane :  public watercraft,   public aircraft {
	protected:
		unsigned putnici;
	public:

		seaplane(unsigned putnik) { putnici = putnik; }
		unsigned passengers() { return putnici; }
		string type() {
			return this->aircraft::tip + this->watercraft::tip;
		}
	};
}