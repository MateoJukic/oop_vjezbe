#pragma once
#include <string>
#include <iostream>
using namespace std;
namespace oop {

	class vehicle {
	public:
		virtual string type() = 0;
		virtual unsigned passengers() = 0;
		virtual ~vehicle() {};
	};
}