#pragma once
#include "vehicle.h"
namespace oop {
	class watercraft :virtual public vehicle {
	protected:
		string tip;
	public:
		watercraft() {
			tip = "water";
		}
		string type() {
			return tip;
		}

	};
}