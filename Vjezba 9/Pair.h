#pragma once
#include <iostream>
template<typename T1, typename T2>
class Pair
{
	T1 first;
	T2 second;
public:
	Pair(const T1& t1 = 'k', const T2& t2 = 'k') {
		this->first = t1;
		this->second = t2;
	}
	bool operator== (const Pair<T1, T2>& other) const
	{
		return first == other.first && second == other.second;
	}
	bool operator <= (const Pair<T1, T2>&other) const {
		return first <= other.first && second <= other.second;
	}
	bool operator < (const Pair<T1, T2>&other) const {
		return first < other.first && second < other.second;
	}
	bool operator > (const Pair<T1, T2>&other) const {
		return first > other.first && second > other.second;
	}
	bool operator >= (const Pair<T1, T2>&other) const {
		return first >= other.first && second >= other.second;
	}
	bool operator != (const Pair<T1, T2>&other) const {
		return first != other.first && second != other.second;
	}
	Pair operator = (const Pair<T1, T2>& other) const {
		Pair<T1, T2> a;
		a.first = other.first;
		a.second = other.second;
		return a;
	}
	friend std::ostream& operator << (std::ostream& out, Pair<T1, T2>& par) {
		out << par.first << std::endl;
		out << par.second << std::endl;
		return out;
	}
	friend std::istream& operator >> (std::istream& in, Pair<T1, T2>& par) {
		std::cout << "Unesite prvi clan para."<<std::endl;
		in >> par.first;
		in >> par.second;
		return in;
	}

	void swap() {
		T1 temp;
		temp.first = this->first;
		this->first = this->second;
		this->second = temp;
	}
};

template <>
class Pair<char*, char*> {
	char* first;
	char* second;
public:
	Pair( char* a = NULL,  char* b = NULL ) {
		first = new char;
		second = new char;
		first = a;
		second = b;
	}

	bool operator== (const Pair<char*, char*>& other) const
	{
		return (this->first==other.first) && (this->second == other.second);
	}
	
	bool operator < (const Pair<char*, char*>& other) const {
		return (this->first < other.first) && (this->second < other.second);

		
	}
	bool operator > (const Pair<char*, char*>& other) const {
		return (this->first > other.first) && (this->second > other.second);
	}

	friend std::ostream& operator << (std::ostream& out, Pair<char*, char*>& par) {
		out << par.first;
		out << par.second << std::endl;
		return out;
	}
	friend std::istream& operator >> (std::istream& in, Pair<char*, char*>& par) {
		char* a =new char;
		char* b =new char;
		in >> a >> b;
		par.first = a;
		par.second = b;
		return in;
	}


};

