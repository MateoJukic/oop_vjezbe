
#include "pch.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

#include "Pair.h"

using namespace std;

int main()
{
	
	Pair<char*, char*> p1, p2, p3;
	vector<Pair<char*, char*> > v;

	cin >> p1;
	cin >> p2;
	cin >> p3;

	v.push_back(p1);
	v.push_back(p2);
	v.push_back(p3);

	sort(v.begin(), v.end());
	cout << endl;
	cout << endl;

	vector<Pair<char*, char*> >::iterator it;
	for (it = v.begin(); it != v.end(); ++it)
		cout << *it << endl;

}